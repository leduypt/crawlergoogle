# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import configparser
from scrapy.utils.project import get_project_settings
import mysql.connector
import hashlib
import json
import happybase
import starbase
import urllib, requests
import pika
import redis
import csv
import os.path
import sys
from starbase import Connection
from datetime import datetime 
from scrapy.conf import settings
reload(sys)
sys.setdefaultencoding('utf8')

class CrawlergooglePipeline(object):
    def __init__(self):
        print "[Info] Init"
        # config.read(settings.get('EXTRA_CONFIG_FILE'))
        # self.extra_config = config

        self.now  = datetime.now()
        self.conn = mysql.connector.connect(user=settings['MYSQL_USERNAME'],
                                    passwd=settings['MYSQL_PASSWD'],
                                    db=settings['MYSQL_DB'],
                                    host=settings['MYSQL_HOST'],
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def mysqConnect(self):
        self.conn = mysql.connector.connect(user=settings['MYSQL_USERNAME'],
                                    passwd=settings['MYSQL_PASSWD'],
                                    db=settings['MYSQL_DB'],
                                    host=settings['MYSQL_HOST'],
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def insertDatabase(self, sql, params=()):
        try:
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except mysql.connector.Error as e:
            print ('exception generated during sql connection: ', e)
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor.lastrowid
 
    def process_item(self, item, spider):
        print "\n"
        print ("==== DEBUG PIPELINE ====")
        print "\n"
        print item
        self._set_raw(item)
        print "\n"
        print "=========================="
        return item

    def _set_raw(self, data):
        # json.dumps(data.__dict__)
        sql = """INSERT INTO dlk_data_google(web_id, web_title, web_lead, web_content, status_time, keyword, web_author, web_author_link, web_image, web_category_name, web_link, web_domain_name, web_crawler_time, web_created, web_tag, check_exists, status, data_raw) VALUE (%s, %s, %s, %s, %s, %s, %s,%s,%s, %s, %s, %s, %s,%s,%s, %s, %s, %s)"""
        try:
            curr = self.cursor.execute(sql,(data['web_id'], data['web_title'], data['web_lead'], data['web_content'], data['status_time'], data['keyword'], data['web_author'], data['web_author_link'], data['web_image'], data['web_category_name'], data['web_link'], data['web_domain_name'], data['web_crawler_time'], data['web_created'], data['web_tag'], 0, 0, json.dumps(data.__dict__)))
            self.conn.commit()
            print "Save success to DB: "+data['web_id']
        except Exception as e:
            print (e)
            print ("[EXCEPTION] exception in set raw")