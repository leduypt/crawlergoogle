# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CrawlerGoogleItem(scrapy.Item):
    # define the fields for your item here like:
    web_id = scrapy.Field()
    web_title = scrapy.Field()
    web_lead = scrapy.Field()
    web_content = scrapy.Field()
    web_image = scrapy.Field()
    web_category_name = scrapy.Field()
    web_category_url = scrapy.Field()
    web_author = scrapy.Field()
    web_author_link = scrapy.Field()
    web_link = scrapy.Field()
    web_domain_name = scrapy.Field()
    web_crawler_time = scrapy.Field()
    web_created = scrapy.Field()
    web_tag = scrapy.Field()
    keyword = scrapy.Field()
    status_time = scrapy.Field()
    pass
