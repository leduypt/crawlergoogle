# -*- coding: utf-8 -*-
__author__ = 'DuyLK'


# //
# //                            _
# //                         _ooOoo_
# //                        o8888888o
# //                        88" . "88
# //                        (| -_- |)
# //                        O\  =  /O
# //                     ____/`---'\____
# //                   .'  \\|     |//  `.
# //                  /  \\|||  :  |||//  \
# //                 /  _||||| -:- |||||_  \
# //                 |   | \\\  -  /'| |   |
# //                 | \_|  `\`---'//  |_/ |
# //                 \  .-\__ `-. -'__/-.  /
# //               ___`. .'  /--.--\  `. .'___
# //            ."" '<  `.___\_<|>_/___.' _> \"".
# //           | | :  `- \`. ;`. _/; .'/ /  .' ; |
# //           \  \ `-.   \_\_`. _.'_/_/  -' _.' /
# // ===========`-.`___`-.__\ \___  /__.-'_.'_.-'================



from datetime import datetime,date
import time
import os
from datetime import timedelta
from  dateutil import parser
import calendar
import unidecode
import re
import requests
import json
from CrawlerGoogle.helper import HelperKeys
from CrawlerGoogle.items import CrawlerGoogleItem

class Helper:
    def __init__(self, *args, **kwargs):
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        self.DATE_REGEX =   '\d+/\d+/\d+'
        self.TIME_REGEX = '\d+:\d+'
        #Telegram Config
        self.bot_token = '1373128266:AAGLzdOlNKAx8TK31XfKzW6Od2mgemsc0Yc'
        self.bot_chatID = '-341791503'

    def getServerVersion(self):
        return "SV01"

    def _join_data(self, separator=None, data=None):
        if(separator != None and data != None):
            if len(data) > 0:
                s = data[0]
                for i in range(1, len(data)):
                    if(data[i]):
                        words = data[i].strip().split(" ")
                        if len(words) > 15:
                            s = s + " <br><br> " + data[i] 
                        else:
                            s = s + separator + data[i].strip()
                s=re.sub(r"(<br>){2,}", " <br><br> ",s)
                s=re.sub(r"\s{2,}", " ",s)
                return s
        return ''

    def _to_dict(self, data):
        d = {}
        if(len(data) > 0):
            for i in range(0, len(data)):
                d[i] = data[i]
        return d

    def createdHeader(self, domain):
        headers = {
            "Host": ""+domain+"",
            "User-Agent" : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36",
            "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Language": "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2,zh-CN;q=0.2,zh;q=0.2,mt;q=0.2,zh-TW;q=0.2",
            "Accept-Encoding": "gzip, deflate",
            "Connection": "keep-alive",
            "Content-Type": "application/x-www-form-urlencoded",
            "Cookie": ""
        }
        return headers

    def getItem(self):
        item = CrawlerGoogleItem()
        item['web_id'] = ""
        item['web_title'] = ""
        item['web_lead'] = ""
        item['web_content'] = ""
        item['web_author'] = ""
        item['web_author_link'] = ""
        item['web_image'] = ""
        item['web_category_name'] = ""
        item['web_link'] = ""
        item['web_domain_name'] = ""
        item['web_crawler_time'] = 0
        item['web_created'] = ""
        item['web_tag'] = ""
        item['keyword'] = ""
        item['status_time'] = 0
        return item


    def format_classic(self,input_str):
        # dateformat example: 20/11/2016
        # time format: 15:00
        input_str = input_str.strip()
        try:
            date_rex = "\d+/\d+/\d+"
            time_rex = "\d+:\d+"
            date_str = re.findall(date_rex,input_str)[0]
            time_str = re.findall(time_rex,input_str)[0]
            stripped = time_str + " " + date_str
            return datetime.strptime(stripped, "%H:%M %d/%m/%Y").strftime(self.DATETIME_FORMAT)
        except Exception:
            print "[EXCEPTION] datetime converting exception, classic1 prototype datetime parsing"
            return datetime.today().strftime(self.DATETIME_FORMAT)

    def format_time(self, input_str):
        #sample string : "2020-12-22T10:59:53+07:00"
        input_str = str(unidecode.unidecode(input_str)).strip()
        print "------------------------------"
        print "Time in: "+input_str
        try:
            date_rex = "\d+-\d+-\d+"
            time_rex = "\d+:\d+:\d+"
            date_str = re.findall(date_rex,input_str)[0]
            time_str = re.findall(time_rex,input_str)[0]
            stripped = time_str + " " + date_str
            print "Time out: "+stripped
            return datetime.strptime(stripped, "%H:%M:%S %Y-%m-%d").strftime(self.DATETIME_FORMAT)
        except Exception:
            # print "[EXCEPTION] datetime converting exception, Not format: 2020-12-22T10:59:53+07:00 "
            try:
                # sample string : "1 gio truoc", "1 ngay truoc"
                stamp = None
                if "phut truoc" in input_str:
                    string = input_str.replace(" phut truoc","")
                    m = int(string)
                    stamp = str(datetime.now() + timedelta(minutes=-m))
                if "gio truoc" in input_str:
                    string = input_str.replace(" gio truoc","")
                    h = int(string)
                    stamp = str(datetime.now() + timedelta(hours=-h))
                if "ngay" in input_str:
                    string = input_str.replace(" ngay truoc","")
                    d = int(string)
                    stamp = str(datetime.now() + timedelta(days=-d))
                if stamp != None:
                    print stamp
                    date_rex = "\d+-\d+-\d+"
                    time_rex = "\d+:\d+:\d+"
                    date_str = re.findall(date_rex,stamp)[0]
                    time_str = re.findall(time_rex,stamp)[0]
                    stripped = time_str + " " + date_str
                    print "Time out: "+datetime.strptime(stripped, "%H:%M:%S %Y-%m-%d").strftime(self.DATETIME_FORMAT)
                    return datetime.strptime(stripped, "%H:%M:%S %Y-%m-%d").strftime(self.DATETIME_FORMAT)
                else:
                    print "[EXCEPTION] datetime converting exception, Format Fail"
                    print "Time out: "+datetime.today().strftime(self.DATETIME_FORMAT)
                    return datetime.today().strftime(self.DATETIME_FORMAT)
            except Exception:
                print "[EXCEPTION] datetime converting exception, None Imput String"
                print "Time out: "+datetime.today().strftime(self.DATETIME_FORMAT)
                return datetime.today().strftime(self.DATETIME_FORMAT)

    def sendTelegram(self, urls=[]):
        print '=>>>>>>>>>>>Send Telegram!'
        dataUrlText = ''
        for url in urls:
            dataUrlText += "\n -"+url +"\n"

        bot_message = " \n Date: "+str(datetime.now())+" \n New item count: "+str(len(urls))+" \n List Urls New:"+dataUrlText+" \n --------------------------------------------------------------------"
        send_text = 'https://api.telegram.org/bot' + self.bot_token + '/sendMessage?chat_id=' + self.bot_chatID + '&parse_mode=Markdown&text=' + bot_message
        response = requests.get(send_text)
        print '[Crawler Google Notification] =>>>>>>>>>>>Send Telegram Success!'

    def sendMessageTelegram(self, message):
        print '=>>>>>>>>>>>Send Message Telegram!'
        send_text = 'https://api.telegram.org/bot' + self.bot_token + '/sendMessage?chat_id=' + self.bot_chatID + '&parse_mode=Markdown&text=' + message
        response = requests.get(send_text)
        print '[Crawler Google Notification] =>>>>>>>>>>>Send Telegram Success!'


    def getDataFormUrl(self, url):
        print '=>>>>>>>>>>>Get Data Form URL!'
        url_request = "http://123.30.186.133:5000/api/v1/resources?url="+url
        response = requests.get(url_request)
        return response.content