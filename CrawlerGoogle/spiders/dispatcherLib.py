__author__ = 'DuyLK'
# -*- coding: utf-8 -*-

import ConfigParser
from scrapy.conf import settings
from datetime import datetime
from datetime import timedelta
import dateutil.parser
import time
import mysql.connector
import os
from helper import Helper
from CrawlerGoogle.helper import HelperKeys
class DispatcherLibrary:
    def __init__(self, *args, **kwargs):
        self.filepath = os.getcwd()
        self.process_id = os.getpid()
        self.limit = 10
        self.arrKeys = HelperKeys().ApiKeyHelper()
        self.table_keys = 'dlk_data_keys'

    def mysqConnect(self):
        self.conn = mysql.connector.connect(user=settings['MYSQL_USERNAME'],
                                    passwd=settings['MYSQL_PASSWD'],
                                    db=settings['MYSQL_DB'],
                                    host=settings['MYSQL_HOST'],
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def mysqlQuery(self, sql, params=()):
        try:
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except mysql.connector.Error as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def spider_open(self):
        print spider_open(self)

    def spider_close(self, reason):
        print spider_close(self)

    def getKeywords(self, status):
        sql = """SELECT id, keyword, object_id, status FROM dlk_data_keywords WHERE status = '%s' """
        curr = self.mysqlQuery(sql % status)
        if curr :
            return curr.fetchall()
        else:
            return []


    def getUrlLeechByGroup(self, group, page):
        start = (page - 1) * self.limit
        sql = """SELECT id, domain_url, category_name, domain_name, domain_group, status, created, pay_position_1, pay_position_2, pay_position_3, pay_position_4, pay_position_5, pay_position_6 FROM web_url WHERE domain_group = %s AND status = 1 ORDER BY id DESC LIMIT %s,%s"""
        curr = self.mysqlQuery(sql,(group, start,self.limit))
        if curr :
            return curr.fetchall()
        else:
            return []

    def insertLog(self, action, message):
        current_time = time.time()
        try:
            sql = """INSERT INTO dlk_data_logs(action, message) VALUE (%s, %s)"""
            curr = self.mysqlQuery(sql,(action, message))
            self.conn.commit()
            print "=>>>>>>>>>>>>>>>>> Save Log"
        except Exception as e:
            print (e)
            print ("[EXCEPTION] exception in save log")

    #Remove Key Limit
    def removeKeyLimit(self, key):
        print "Remove Key Limit"
        # REMOVE KEY IN ARRAY
        self.arrKeys.remove(key)
        # UPDATE STATUS KEYS
        self._set_status_key(0,key)
        # SAVE FILE HELPER
        temp_path = self.filepath + '/' + 'CrawlerGoogle' + '/' + 'helper.py'
        file_object = open(temp_path, "r")
        info_object = file_object.read()
        file_object.close()

        string = info_object.split(" = [")[1].split("]")[0]

        new_string = "\n"
        for key in self.arrKeys:
            new_string = new_string + '\t\t\t"'+key+'",\n'

        file_object = open(temp_path, "w")
        file_object.write(info_object.replace(string, new_string))
        file_object.close()

    def _set_status_key(self, status, key):
        sql = """UPDATE """ + self.table_keys + """ SET `status` = %s WHERE `key` = %s"""
        curr = self.mysqlQuery(sql,(status,key))