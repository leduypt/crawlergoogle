# -*- coding: utf-8 -*-
__author__ = 'DuyLK'

import scrapy
import json
import time
import mysql.connector
import re
import os 
import redis
import hashlib
import urllib
import requests
import random
from scrapy.exceptions import CloseSpider
from elasticsearch import Elasticsearch
from datetime import datetime,date
from datetime import timedelta
from scrapy import signals
from scrapy.conf import settings
from scrapy.xlib.pydispatch import dispatcher
from unidecode import unidecode
from urlparse import urlparse
from helper import Helper
from scrapy import Spider, Request
from scrapy.selector import Selector
from dispatcherLib import DispatcherLibrary
from CrawlerGoogle.helper import HelperKeys
from CrawlerGoogle.items import CrawlerGoogleItem

class GoogleSpider(scrapy.Spider):
    name = 'google'
    allowed_domains = ['customsearch.googleapis.com', 'api.duylk.work']
    handle_httpstatus_list = [403, 404, 200, 190, 100, 400]
    def __init__(self, timeGet=1, cx="97fff41ad3e46434f", numPost=10, *args, **kwargs):
        now = datetime.now()
        # dd/mm/YY H:M:S
        timeFormat = now.strftime("%d/%m/%Y %H:%M:%S")
        print "Run Google API Crawler: "+timeFormat
        self.cx = cx
        self.numPost = str(numPost)
        self.timeGet = int(timeGet)
        self.startIndex = [0,10]
        self.itemNew = []
        self.keyword = ''
        self.googleApiKey = ''
        self.dateNow = date.today()
        self.es_doc_type = 'filtered'
        self.es_index = 'website'
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        print '[INFO] Init connection mysql'
        #ES Config
        self.es = Elasticsearch([{'host': settings['ES_HOST'] , 'port': settings['ES_PORT']}])
        #MySQL Config
        self.core_conn = mysql.connector.connect(user=settings['MYSQL_USERNAME'],
                                    passwd=settings['MYSQL_PASSWD'],
                                    db=settings['MYSQL_DB'],
                                    host=settings['MYSQL_HOST'],
                                    charset="utf8", use_unicode=True)
        self.core_cursor= self.core_conn.cursor()
        #Redis Config
        self.redis_db = redis.Redis(host=settings['REDIS_HOST'], port=settings['REDIS_PORT'], db=settings['REDIS_DB_ID'])
        self.redis_google_db = redis.Redis(host=settings['REDIS_HOST'], port=settings['REDIS_PORT'], db=settings['REDIS_DB_GOOGLE_ID'])
        #Func Open Close
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        dispatcher.connect(self.spider_opened, signals.spider_opened)

    def start_requests(self):
        if self.countKey() > 0:
            # Config URL
            dateEnd  = self.dateNow.strftime('%Y%m%d')
            dateStart = (self.dateNow - timedelta(days=self.timeGet)).strftime('%Y%m%d')
            self.dateRange = dateStart+':'+dateEnd
            self.fields = 'queries/request(searchTerms,count,totalResults,startIndex),items(title,link,displayLink,snippet,pagemap(cse_thumbnail(src),metatags(og:title,og:description,og:image,article:published_time,author,article:tag)))'

            list_data = DispatcherLibrary().getKeywords(1)
            # print list_data
            if len(list_data) > 0:
                for row in list_data:
                    id, keyword, object_id, status = row
                    self.googleApiKey = self.getKeyRandom()
                    self.keyword = keyword
                    for index in self.startIndex:
                        if self.countKey() > 0:
                            print "KEY1===========>"+self.googleApiKey
                            # url = 'http://api.duylk.work/data.json'
                            keyword_len = keyword.split(" ")
                            if len(keyword_len) < 3:
                                url = 'https://customsearch.googleapis.com/customsearch/v1?q='+self.keyword+'&cx='+self.cx+'&num='+self.numPost+'&key='+self.googleApiKey+'&fields='+self.fields+'&alt=json&sort=date:r:'+self.dateRange+'&start='+str(index)+'&gl=vn&hl=vi&filter=0'
                            else:
                                url = 'https://customsearch.googleapis.com/customsearch/v1?exactTerms='+self.keyword+'&cx'+self.cx+'&num='+self.numPost+'&key='+self.googleApiKey+'&fields='+self.fields+'&alt=json&sort=date:r:'+self.dateRange+'&start='+str(index)+'&gl=vn&hl=vi&filter=0'
                            meta = {"keyword": keyword, "object_id": object_id, "status": status, 'google_api_key': self.googleApiKey}
                            print "==========================="
                            print "BEGIN CRAWLER GOOGLE API: " + url
                            print "==========================="
                            print "=>>>>>>>>>> Keyword: "+keyword+" - StartIndex:"+str(index)+" <<<<<<<<<<<<<<="
                            # time.sleep(2)
                            yield Request(url=url,meta=meta,callback=self.parse)
                        else:
                            print "=============== Google API Key -> Empty =================="
                            Helper().sendMessageTelegram('=>>>>>>>>>> Google API Key -> Empty')
        else:
            Helper().sendMessageTelegram('=>>>>>>>>>> Google API Key -> Empty')
            print "=============== Google API Key -> Empty =================="

    def parse(self, response):
        print "[INFO] Spider Parse"
        print response.status
        #Response Meta
        metas = response.meta
        keyword = response.meta["keyword"]
        object_id = response.meta["object_id"]
        google_api_key = response.meta["google_api_key"]

        #API Error
        if response.status == 400:
            print "[ERROR] 400 API : " + response.url

        if response.status == 403 or response.status == 404:
            print "[ERROR] 403 - Custom Search API has not been used in project : " + response.url
            print "=============== Google API Key -> Die =================="
            # print self.countKey()
            if self.countKey() > 0:
                print "KEY1===========>"+self.googleApiKey
                DispatcherLibrary().removeKeyLimit(google_api_key)
                self.googleApiKey = self.getKeyRandom()
                self.startNew = '0'
                metas['google_api_key'] = self.googleApiKey
                newUrl = 'https://customsearch.googleapis.com/customsearch/v1?q='+self.keyword+'&cx='+self.cx+'&num='+self.numPost+'&key='+self.googleApiKey+'&fields='+self.fields+'&alt=json&sort=date:r:'+self.dateRange+'&start='+self.startNew+'&gl=vn&hl=vi&filter=0'
                print "=>>>>>>>>>>>> Again Request to Google API <<<<<<<<<<<<<<="
                yield Request(url=newUrl, meta=metas, callback=self.parse)
            else:
                print "=============== Google API Key -> Empty =================="
                Helper().sendMessageTelegram('=>>>>>>>>>> Google API Key -> Empty')

        #API Sucess
        if response.status == 200:
            #Get all posts
            datas = json.loads(response.body)
            if "items" in datas:
                posts = datas['items']
                if len(posts) > 0:
                    print "=============== FOR POSTS =================="
                    for post in posts:
                        #Key Hash
                        string = str(post['link'].encode('utf-8')) + "_" + str(post['displayLink'])
                        key_hash = hashlib.md5(str(string).decode('utf-8').encode('utf-8')).hexdigest()

                        # Check Duplicate
                        duplicate = self.redis_exists(key_hash)
                        # duplicate = es_exists(key_hash)
                        if duplicate == True:
                            print "[INFO] ITEM ALREADY EXISTS DON'T REQUEST AGAIN"
                            print "[INFO] THE KEY: " + key_hash
                            print "[INFO] THE WEB LINK : " + str(post['link'].encode("utf-8"))
                        else:
                            print "============= POST NOT DUPLICATE ============="
                            print post['link']
                            print "=============================================="

                            self.itemNew.append(post['link'])
                            #Insert Key to Redis
                            self.insert_key_to_redis(key_hash)

                            #Object Item
                            item = Helper().getItem()
                            item['web_id'] = key_hash
                            item['web_link'] = post['link']
                            item['web_domain_name'] = post['displayLink']
                            item['web_crawler_time'] = datetime.today().strftime(self.DATETIME_FORMAT)
                            item['web_author_link'] = ""
                            item['web_content'] = ""
                            item['web_category_name'] = ""
                            item['web_tag'] = ""

                            #Content From URL
                            dataFromUrl = Helper().getDataFormUrl(item['web_link'])
                            data_json = json.loads(dataFromUrl)
                            if data_json and data_json['status_code'] == 200:
                                item['web_content'] = data_json['data']['content']
                                item['web_title'] = data_json['data']['title']
                                # item['web_created'] = self.format_datetime("data_json['data']['time']")
                            else:
                                print "=>>>>>>>>>>>> Request Fail: http://123.30.186.133:5000 <<<<<<<<<<<<<<="
                            
                            #Keyword
                            item['keyword'] = keyword

                            #If Meta
                            if "pagemap" in post:
                                if "metatags" in post['pagemap']:
                                    metas =  post['pagemap']['metatags'][0]
                                    if "article:published_time" in metas:
                                        item['web_created'] = self.format_datetime(str(metas['article:published_time']))
                                        item['status_time'] = 1
                                    else:
                                        input_str = str(unidecode(post['snippet'])).strip()
                                        if re.findall("\d+ \w+ truoc",input_str):
                                            datetime_str = str(re.findall("\d+ \w+ truoc",input_str)[0])
                                            item['web_created'] = self.format_datetime(str(datetime_str))
                                            item['status_time'] = 2
                                        else:
                                            item['web_created'] = self.format_datetime("")

                                    if "og:title" in metas:
                                        if metas['og:title'] is not None:
                                            item['web_title'] = metas['og:title'].encode('utf-8')
                                        else: 
                                            item['web_title'] = post['title'].encode('utf-8')
                                    else:
                                        item['web_title'] = post['title'].encode('utf-8')

                                    if "og:description" in metas:
                                        item['web_lead'] = metas['og:description'].encode('utf-8')
                                    else:
                                        item['web_lead'] = post['snippet'].encode('utf-8')

                                    if "author" in metas:
                                        item['web_author'] = metas['author']
                                    else:
                                        item['web_author'] = ""

                                    if "og:image" in metas:
                                        item['web_image'] = metas['og:image']
                                    else:
                                        item['web_image'] = ""   

                                    if "article:tag" in metas:
                                        item['web_tag'] = metas['article:tag']      
                                else:
                                    item['web_created'] = self.format_datetime("")

                                if item['web_created'] is None:
                                    item['web_created'] = self.format_datetime("")

                                if "cse_thumbnail" in post['pagemap'] and item['web_image'] == "" :
                                    item['web_image'] = post['pagemap']['cse_thumbnail'][0]['src']
                                
                            # else:
                            #     input_str = str(unidecode(post['snippet'])).strip()
                            #     datetime_str = str(re.findall("\d+ \w+ truoc",input_str)[0])
                            #     item['web_created'] = self.format_datetime(str(datetime_str))
                            #     if datetime_str:
                            #         item['status_time'] = 2
                            #     item['web_title'] = post['title'].encode('utf-8')
                            #     item['web_lead'] = post['snippet'].encode('utf-8')
                            #     item['web_author'] = ""
                            #     item['web_image'] = ""   
                            
                            yield item
                    # print(items)
                else:
                    print "=============== Empty Post =================="    
            else:
                print "=============== Empty Post - Next Page =================="                         


    #Check Redis
    def redis_exists(self,key):
        val = self.redis_google_db.get(key)
        if val == "exist":
            return True
        return False

    #Insert Key to Redis
    def insert_key_to_redis(self, key):
        nano = self.redis_google_db.set(key,"exist")

        print ("================= SET =================")
        print ("=>>>>>>>>>>>> Key: " + key)
        print (nano)
        print ("=======================================")

    #ES check Exists
    def es_exists(self, key):
        result = self.es.search(index=self.es_index, doc_type=self.es_doc_type, body={
            "query": {
                "match": {
                    "web_id": key
                }
            }
        })
        # print result['hits']['hits']
        if len(result['hits']['hits']) > 0:
            return True
        else:
            return False

    #Format Time
    def format_datetime(self,datetime_str):
        #Helper().format_time()
        funcName = "format_time"
        datetime_formater = getattr(Helper(),funcName)
        return datetime_formater(datetime_str)

    #Get (Google API Key) Random
    def getKeyRandom(self):
        keys = HelperKeys().ApiKeyHelper()
        if len(keys) > 0:
            return keys[random.randrange(0, len(keys))]
        else:
            return ''

    #Count Key Active
    def countKey(self):
        keys = HelperKeys().ApiKeyHelper()
        return len(keys)

    #Spider Open Func
    def spider_opened(self,spider):
        print "[INFO] Spider Open"
        DispatcherLibrary().insertLog('open', 'Run Spider' )

    #Spider Close Func
    def spider_closed(self, spider, reason):
        print "[INFO] Spider Close!"
        DispatcherLibrary().insertLog('close', 'Run Spider Finish' )
        if len(self.itemNew) > 0:
            Helper().sendTelegram(self.itemNew)
